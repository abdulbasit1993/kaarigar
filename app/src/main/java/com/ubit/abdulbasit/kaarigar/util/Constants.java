package com.ubit.abdulbasit.kaarigar.util;

public class Constants {
    public final static String PUBNUB_PUBLISH_KEY = "pub-c-6705a83d-2f4b-4c1d-ab8d-2f30b2bb2cf2";
    public final static String PUBNUB_SUBSCRIBE_KEY = "sub-c-3ce4a6b2-141d-11e9-923b-9ef472141036";
    public final static String PUBNUB_CHANNEL_NAME = "drivers_location";
}